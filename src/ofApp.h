#pragma once

#include "ofxKinectForWindows2.h"

#include "ofMain.h"
#include "ofxNDI.h" 
#include "ofxGui.h"
#include "KinectThreshold.h"
#include "KinectPointCloud.h"
#include "KinectParticleSystem.h"
#include "KinectFrameDifference.h"
#include "FluidFlow.h"
#include "KinectRGB.h"


class ofApp : public ofBaseApp {

public:
	void setup();
	void setupGui();
	void update();
	void draw();

	void keyPressed(int key);
	void keyReleased(int key);
	void mouseMoved(int x, int y);
	void mouseDragged(int x, int y, int button);
	void mousePressed(int x, int y, int button);
	void mouseReleased(int x, int y, int button);
	void mouseEntered(int x, int y);
	void mouseExited(int x, int y);
	void windowResized(int w, int h);
	void dragEvent(ofDragInfo dragInfo);
	void gotMessage(ofMessage msg);

	void totalParticlesChanged(int& totalParticles);
	void totalRectanglesChanged(int& totalRectangles);
	void widthRectanglesChanged(int& widthRectangles);
	void saveButtonPressed();

	ofxKFW2::Device kinect;
	KinectThreshold kinectThreshold;
	KinectPointCloud kinectPointCloud;
	KinectParticleSystem kinectParticleSystem;
	KinectFrameDifference kinectFrameDifference;
	FluidFlow fluidFlow;
	KinectRGB kinectRGB;

	int depthWidth;
	int depthHeight;

	// GUI
	ofxPanel gui;
	ofParameterGroup thresholdParams;
	ofParameterGroup pointCloudParams;
	ofParameterGroup particleSystemParams;
	ofParameterGroup frameDifferenceParams;
	ofParameterGroup fluidFlowParams;
	ofParameterGroup kinectRGBParams;



	//ofParameter<glm::vec2> threshold;
	ofParameter<bool> showKinectThreshold;
	ofParameter<int> _near;
	ofParameter<int> _far;

	///
	ofParameter<bool> showKinectPointCloud;
	ofParameter<int> depthMax;
	ofParameter<int> step;
	ofParameter<float> pointSize;
	//ofParameter<bool> showMesh;

	///
	ofParameter<bool> showParticleSystem;
	ofParameter<int> distance;
	ofParameter<int> alpha;
	ofParameter<bool> showDots;
	ofParameter<int> totalParticles;

	/// frameDifference
	ofParameter<bool> showFrameDifference;
	ofParameter<bool> showDifferencePixels;
	ofParameter<int> totalRectangles;
	ofParameter<int> widthRectangles;
	ofParameter<int> alphaDecay;
	ofParameter<float> minPixelsToTrigger;
	ofParameter<int> baseAlpha;
	ofParameter<int> midiStep;

	ofParameter<bool> showFluidFlow;
	ofParameter<bool> showFluidParameters;
	ofParameter<bool> showDepth;

	ofParameter<bool> showKinectRGB;




	ofxButton saveButton;

	ofColor color1a;
	ofColor color1b;
	ofColor color2a;
	ofColor color2b;
	ofColor color3a;
	ofColor color3b;
	ofColor color4a;
	ofColor color4b;



	ofVideoGrabber webcam;
	ofFbo fboWebcam;
	ofxNDIsender ndiWebcam;
	int webcamWidth;
	int webcamHeight;

};
