#include "KinectParticleSystem.h"

Particle::Particle() {}




void Particle::setup(ofVec2f pos) {
	position = pos;
	float scale = 0.1;
	velocity.set(ofRandom(-1, 1) * scale, ofRandom(-1, 1) * scale);

}

void Particle::update(int width, int height) {
	position += velocity;
	if (position.x < 0) position.x = 0;
	if (position.x > width) position.x = width;
	if (position.y < 0) position.y = 0;
	if (position.y > height) position.y = height;
}

void Particle::draw() {
	ofDrawCircle(position, 1);
}

/// /////////////////////////////////////////////////////////////////
KinectParticleSystem::KinectParticleSystem() {}

void KinectParticleSystem::setup(int _width, int _height, string ndiSenderName, int _totalParticles) {
	width = _width;
	height = _height;

	createParticles(_totalParticles);

	fbo.allocate(width, height, GL_RGBA);
	ndiSender.CreateSender(ndiSenderName.c_str(), width, height);

}
void KinectParticleSystem::createParticles(int amount) {
	particles.clear();
	numParticles = amount;
	for (int i = 0; i < numParticles; i++) {
		Particle p;
		p.setup(ofVec2f(ofRandom(width), ofRandom(height)));
		particles.push_back(p);
	}
}

void KinectParticleSystem::update(int _distance) {
	distance = _distance;
	for (Particle& particle : particles) {
		particle.update(width, height);

		if (particle.position.x == 0 || particle.position.x == width) {
			particle.velocity.x *= -1;
		}
		if (particle.position.y == 0 || particle.position.y == height) {
			particle.velocity.y *= -1;
		}
	}
}

void KinectParticleSystem::draw(int x, int y, ofTexture& processedTexture, int _alpha, bool _showDots) {
	ofPixels pixels;
	processedTexture.readToPixels(pixels);

	fbo.begin();
	ofClear(0, 0, 0, 255);

	if (_showDots) {
		for (Particle& p : particles) {
			p.draw();
		}
	}


	ofSetColor(255, _alpha);
	ofEnableBlendMode(OF_BLENDMODE_ADD);
	for (int i = 0; i < particles.size(); i++) {
		Particle& p1 = particles[i];
		glm::vec2 pos1 = glm::vec2(p1.position.x, p1.position.y);

		if (pos1.x < 0) continue;
		if (pos1.x >= pixels.getWidth()) continue;
		if (pos1.y < 0) continue;
		if (pos1.y >= pixels.getHeight()) continue;

		ofColor color1 = pixels.getColor(pos1.x, pos1.y);

		if (color1 == ofColor::black) continue;

		for (int j = i + 1; j < particles.size(); j++) {
			Particle& p2 = particles[j];
			glm::vec2 pos2 = glm::vec2(p2.position.x, p2.position.y);

			if (pos2.x < 0) continue;
			if (pos2.x >= pixels.getWidth()) continue;
			if (pos2.y < 0) continue;
			if (pos2.y >= pixels.getHeight()) continue;

			ofColor color2 = pixels.getColor(pos2.x, pos2.y);

			if (color2 == ofColor::black) continue;

			float dist = glm::distance(pos1, pos2);
			if (dist < distance) {
				ofDrawLine(p1.position, p2.position);
			}
		}
	}
	ofSetColor(255);
	ofDisableBlendMode();




	fbo.end();
	fbo.draw(x, y);

	ndiSender.SendImage(fbo);
}
