#include "KinectFrameDifference.h"

KinectFrameDifference::KinectFrameDifference() {
}

//HACER:
/*
Parametros q faltan:
-poder setear el rango de colores del threshold (degrade?)
-el color minimo de los rectangles
poder mover la camara del pointcloud

mandar midi
*/

void KinectFrameDifference::setup(int width, int height, string ndiSenderName) {
	processedPixels.allocate(width, height, OF_IMAGE_GRAYSCALE);
	previousPixels.allocate(width, height, OF_IMAGE_GRAYSCALE);
	differencePixels.allocate(width, height, OF_IMAGE_GRAYSCALE);

	//previousPixels.set(0);
	processedTexture.allocate(processedPixels);
	processedTexture2.allocate(previousPixels);


	senderName = ndiSenderName;
	fbo.allocate(width, height, GL_RGBA);
	ndiSender.SetReadback();
	ndiSender.SetAsync();
	ndiSender.CreateSender(senderName.c_str(), width, height);

	createRectangles(10, 50);

	int port = 0;
	vector<string> portNames = midiOut.getOutPortList();
	for (size_t i = 0; i < portNames.size(); i++) {
		if (portNames[i].find("LoopBe") != string::npos) {
			port = i;
			break;
		}
	}
	midiOut.openPort(port);

}
void KinectFrameDifference::createRectangles(int amount, int width) {
	rectangles.clear();
	rectAlpha.clear();
	int rectWidth = differencePixels.getWidth() / amount;
	for (int i = 0; i < amount; i++) {
		ofRectangle rect;
		rect.x = i * rectWidth;
		rect.y = 0;
		rect.width = rectWidth * float((float(width) / 100));
		rect.height = differencePixels.getHeight();
		rectangles.push_back(rect);

		rectAlpha.push_back(rectAlphaMin);

		rectTriggered.push_back(false);

	}
}

void KinectFrameDifference::update(ofPixels_<unsigned short>& depthPixels, int _near, int _far, const int& alphaDecay, const float& minPixelsToTrigger, int _rectAlphaMin, int midiStep) {
	int _near2 = _near * 10;
	int _far2 = _far * 10;
	int height = depthPixels.getHeight();
	int width = depthPixels.getWidth();
	int step = 2;
	rectAlphaMin = _rectAlphaMin;

	for (int y = 0; y < height; y += step) {
		for (int x = 0; x < width; x += step) {
			unsigned short depthValue = depthPixels.getColor(x, y).getBrightness();
			if (depthValue > _near2 && depthValue < _far2) {
				processedPixels.setColor(x, y, ofColor(255));
			}
			else {
				processedPixels.setColor(x, y, ofColor(0));
			}
		}
	}
	const unsigned short DIFF_THRESHOLD = 10;
	for (int y = 0; y < height; y += step) {
		for (int x = 0; x < width; x += step) {
			unsigned short currentBrightness = processedPixels.getColor(x, y).getBrightness();
			unsigned short prevBrightness = previousPixels.getColor(x, y).getBrightness();

			if (abs(currentBrightness - prevBrightness) > DIFF_THRESHOLD) {
				differencePixels.setColor(x, y, ofColor(255));
			}
			else {
				differencePixels.setColor(x, y, ofColor(0));
			}
		}
	}

	for (int i = 0; i < rectAlpha.size(); i++) {
		rectAlpha[i] = rectAlpha[i] - alphaDecay;
		if (rectAlpha[i] <= rectAlphaMin) {
			rectAlpha[i] = rectAlphaMin;
			if (rectTriggered[i] == true) {
				rectTriggered[i] = false;
				midiOut.sendNoteOff(1, i * midiStep);
			}
		}
	}

	if (firstBoot <= 0) {
		/// TRIGGER
		for (int i = 0; i < rectangles.size(); i++) {
			if (isRectTriggered(rectangles[i], rectAlpha[i], differencePixels, minPixelsToTrigger)) {
				rectAlpha[i] = 255;
				rectTriggered[i] = true;
				int pitch = i * midiStep;
				midiOut.sendNoteOn(1, pitch, 127);
			}
		}
	}
	else {
		//firstBoot = false;
		firstBoot--;
	}


	for (int y = 0; y < height; y += step) {
		for (int x = 0; x < width; x += step) {
			unsigned short depthValue = processedPixels.getColor(x, y).getBrightness();
			previousPixels.setColor(x, y, ofColor(depthValue));
		}
	}
	processedTexture.loadData(differencePixels);
	//processedTexture2.loadData(previousPixels);

}

void KinectFrameDifference::draw(int x, int y, bool showDifferencePixels) {
	fbo.begin();
	ofClear(0, 0, 0, 255);

	if (showDifferencePixels) {
		processedTexture.draw(0, 0);
	}

	for (int i = 0; i < rectangles.size(); i++) {
		ofSetColor(255, rectAlpha[i]);
		ofDrawRectangle(rectangles[i]);
	}

	fbo.end();
	fbo.draw(x, y);
	//processedTexture2.draw(220, 430);
}

void KinectFrameDifference::send() {
	ndiSender.SendImage(fbo);
}

bool KinectFrameDifference::isRectTriggered(const ofRectangle& rect, const int& alpha, const ofPixels& differenceImage, const float& minChangedPixels) {
	int count = 0;
	bool res = false;
	int totalPixels = rect.getArea();
	int minPixels = totalPixels * float((float(minChangedPixels)/100));
	if (alpha <= rectAlphaMin) {
		for (int y = rect.y; y < rect.y + rect.height; y++) {
			for (int x = rect.x; x < rect.x + rect.width; x++) {
				if (differenceImage.getColor(x, y).getBrightness() > 0) {
					count++;
					if (count > minPixels) {
						res = true;
						break;
					}
				}
			}
			if (res == true) break;
		}
	}
	return res;
}
