#pragma once

#include "ofMain.h"
#include "ofxNDISender.h"

class Particle {
public:
	Particle();
	void setup(ofVec2f position);
	void update(int width, int height);
	void draw();

	ofVec2f position;
	ofVec2f velocity;
};

class KinectParticleSystem {
public:
	KinectParticleSystem();
	void setup(int width, int height, string ndiSenderName, int _totalParticles);
	void createParticles(int amount);
	void update(int distance);
	void draw(int x, int y, ofTexture& processedTexture, int _alpha, bool _showDots);
	ofFbo fbo;
	ofxNDIsender ndiSender;

	int width, height;
	int distance;
private:
	std::vector<Particle> particles;
	int numParticles = 500;

};
