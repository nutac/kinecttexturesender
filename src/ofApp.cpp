#include "ofApp.h"
#include "ofxGui.h"

//--------------------------------------------------------------
void ofApp::setup() {
	kinect.open();
	kinect.initDepthSource();
	kinect.initColorSource();
	//kinect.initInfraredSource();
	//kinect.initBodySource();
	//kinect.initBodyIndexSource();

	setupGui();

	depthWidth = 512;
	depthHeight = 428;
	kinectThreshold.setup(depthWidth, depthHeight, "KinectThreshold");
	kinectPointCloud.setup(depthWidth, depthHeight, "kinectPointCLoud");

	kinectParticleSystem.setup(depthWidth, depthHeight, "kinectParticleSystem", totalParticles);

	kinectFrameDifference.setup(depthWidth, depthHeight, "KinectFrameDifference");
	kinectFrameDifference.createRectangles(totalRectangles, widthRectangles);

	fluidFlow.setup(depthWidth, depthHeight, "fluidFlow");

	kinectRGB.setup(depthWidth, depthHeight, "kinectRGB");


	///  webcam

	vector<ofVideoDevice> devices = webcam.listDevices();
	for (size_t i = 0; i < devices.size(); i++) {
		if (devices[i].bAvailable) {
			//log the device
			ofLogNotice() << devices[i].id << ": " << devices[i].deviceName;
		}
		else {
			//log the device and note it as unavailable
			ofLogNotice() << devices[i].id << ": " << devices[i].deviceName << " - unavailable ";
		}
	}
	webcamWidth = 640;
	webcamHeight = 480;
	webcam.setDeviceID(2);
	webcam.setDesiredFrameRate(30);
	webcam.initGrabber(720, 480);
	fboWebcam.allocate(webcamWidth, webcamHeight, GL_RGBA);
	string  webcamNdiSender = "webcam";
	ndiWebcam.CreateSender(webcamNdiSender.c_str(), webcamWidth, webcamHeight);
}
void ofApp::setupGui() {
	totalParticles.addListener(this, &ofApp::totalParticlesChanged);
	totalRectangles.addListener(this, &ofApp::totalRectanglesChanged);
	widthRectangles.addListener(this, &ofApp::widthRectanglesChanged);
	saveButton.addListener(this, &ofApp::saveButtonPressed);


	thresholdParams.setName("Kinect Threshold");
	thresholdParams.add(showKinectThreshold.set("ShowKinectThreshold", true));
	thresholdParams.add(_near.set("near", 50, 0, 700));
	thresholdParams.add(_far.set("far", 200, 0, 700));

	pointCloudParams.setName("Kinect Point Cloud");
	pointCloudParams.add(showKinectPointCloud.set("ShowKinectPointCloud", true));
	pointCloudParams.add(depthMax.set("Depth Max", 100, 0, 500));
	pointCloudParams.add(step.set("Step", 4, 1, 10));
	pointCloudParams.add(pointSize.set("Point Size", 2, 1, 10));

	particleSystemParams.setName("Particle System");
	particleSystemParams.add(showParticleSystem.set("ShowParticleSystem", true));
	particleSystemParams.add(distance.set("Distance", 50, 0, 100));
	particleSystemParams.add(alpha.set("Alpha", 80, 0, 255));
	particleSystemParams.add(showDots.set("Show dots", true));
	particleSystemParams.add(totalParticles.set("Total Particles", 500, 100, 3000));

	frameDifferenceParams.setName("Frame Difference");
	frameDifferenceParams.add(showFrameDifference.set("ShowFrameDifference", true));
	frameDifferenceParams.add(showDifferencePixels.set("showDifferencePixels", true));
	frameDifferenceParams.add(totalRectangles.set("Total Rectangles", 10, 5, 20));
	frameDifferenceParams.add(widthRectangles.set("Width Rectangles", 50, 10, 100));
	frameDifferenceParams.add(alphaDecay.set("Alpha Decay", 4, 1, 100));
	//frameDifferenceParams.add(minPixelsToTrigger.set("minPixelsToTrigger", 50, 10, 500));
	frameDifferenceParams.add(minPixelsToTrigger.set("PctPixelsToTrigger", 5, 0.1, 10));
	frameDifferenceParams.add(baseAlpha.set("baseAlpha", 50, 0, 200));
	frameDifferenceParams.add(midiStep.set("midiStep", 2, 1, 12));

	fluidFlowParams.setName("Fluid Flow");
	fluidFlowParams.add(showFluidFlow.set("ShowFluidFlow", true));
	fluidFlowParams.add(showFluidParameters.set("ShowFluidParameters", false));

	kinectRGBParams.setName("kinect RGB");
	kinectRGBParams.add(showKinectRGB.set("showKinectRGB", true));


	gui.setup();

	//color1a = ofColor(0, 24, 143);
	color1a = ofColor(36, 160, 255);
	color1b = color1a.getLerped(ofColor::black, 0.3);
	//color2a = ofColor(0, 178, 148);
	color2a = ofColor(206, 24, 250);
	color2b = color2a.getLerped(ofColor::black, 0.3);
	//color3a = ofColor(236, 0, 140);
	color3a = ofColor(255, 62, 101);
	color3b = color3a.getLerped(ofColor::black, 0.3);
	//color4a = ofColor(0, 178, 148);
	color4a = ofColor(0, 205, 190);
	color4b = color4a.getLerped(ofColor::black, 0.3);

	gui.setHeaderBackgroundColor(color1b);
	gui.setBorderColor(ofColor(50));
	//gui.setDefaultBackgroundColor(lightBaseColor);
	//gui.setBackgroundColor(baseColor);

	gui.add(saveButton.setup("Guardar"));


	gui.setDefaultHeaderBackgroundColor(color1a);
	gui.setDefaultFillColor(color1b);
	gui.add(thresholdParams);

	gui.setDefaultHeaderBackgroundColor(color2a);
	gui.setDefaultFillColor(color2b);
	gui.add(pointCloudParams);

	gui.setDefaultHeaderBackgroundColor(color3a);
	gui.setDefaultFillColor(color3b);
	gui.add(particleSystemParams);

	gui.setDefaultHeaderBackgroundColor(color4a);
	gui.setDefaultFillColor(color4b);
	gui.add(frameDifferenceParams);

	gui.setDefaultHeaderBackgroundColor(color1a);
	gui.setDefaultFillColor(color1b);
	gui.add(fluidFlowParams);

	gui.setDefaultHeaderBackgroundColor(color2a);
	gui.setDefaultFillColor(color2b);
	gui.add(kinectRGBParams);

	gui.loadFromFile("settings.xml");
}
//--------------------------------------------------------------
void ofApp::update() {
	kinect.update();

	ofPixels_<unsigned short>& depthPixels = kinect.getDepthSource()->getPixels();
	if (showKinectThreshold) {
		kinectThreshold.update(depthPixels, _near, _far);
	}

	if (showKinectPointCloud) {
		kinectPointCloud.update(depthPixels, _near, _far, depthMax, step, pointSize);
	}
	if (showParticleSystem) {
		kinectParticleSystem.update(distance);
	}
	if (showFrameDifference) {
		kinectFrameDifference.update(depthPixels, _near, _far, alphaDecay, minPixelsToTrigger, baseAlpha, midiStep);
	}

	if (showFluidFlow) {
		fluidFlow.update(depthPixels, _near, _far);
	}

	webcam.update();

}

//--------------------------------------------------------------
void ofApp::draw() {

	ofBackground(50);

	int left = 220;
	int top2 = 435;
	if (showKinectThreshold) {
		kinectThreshold.draw(left, 0);
		kinectThreshold.send();
	}

	if (showKinectPointCloud) {
		kinectPointCloud.draw(left + (depthWidth + 10) * 1, 0);
		kinectPointCloud.send();
	}

	if (showParticleSystem) {
		kinectParticleSystem.draw(left + (depthWidth + 10) * 2, 0, kinectThreshold.processedTexture, alpha, showDots);
	}

	if (showFrameDifference) {
		kinectFrameDifference.draw(left, top2, showDifferencePixels);
		kinectFrameDifference.send();
	}
	if (showFluidFlow) {
		fluidFlow.draw(220 + (depthWidth + 10) * 1, top2, showFluidParameters);
		fluidFlow.send();
	}

	if (showKinectRGB) {
		kinectRGB.draw(220 + (depthWidth + 10) * 2, top2, kinect.getColorSource()->getTexture());
		kinectRGB.send();
	}




	fboWebcam.begin();
	//webcam.draw(0, 0, webcamWidth, webcamHeight);
	webcam.draw(0, 0);

	fboWebcam.end();
	fboWebcam.draw(left + (depthWidth + 10) * 1, 0, depthWidth, depthHeight);
	ndiWebcam.SendImage(fboWebcam);

	gui.draw();
}
void ofApp::totalParticlesChanged(int& totalParticles) {
	kinectParticleSystem.createParticles(totalParticles);
}
void ofApp::totalRectanglesChanged(int& totalRectangles) {
	kinectFrameDifference.createRectangles(totalRectangles, widthRectangles);
}
void ofApp::widthRectanglesChanged(int& widthRectangles) {
	kinectFrameDifference.createRectangles(totalRectangles, widthRectangles);

}
void ofApp::saveButtonPressed() {
	gui.saveToFile("settings.xml");
	fluidFlow.gui.saveToFile("settings2.xml");

}


//--------------------------------------------------------------
void ofApp::keyPressed(int key) {
	if (key == 's') {
		gui.saveToFile("settings.xml");
		fluidFlow.gui.saveToFile("settings2.xml");
	}
	//if (key == 'g') {
	//	fluidFlow.showGui = !fluidFlow.showGui;
	//}
}

//--------------------------------------------------------------
void ofApp::keyReleased(int key) {

}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y) {

}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button) {

}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button) {

}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button) {

}

//--------------------------------------------------------------
void ofApp::mouseEntered(int x, int y) {

}

//--------------------------------------------------------------
void ofApp::mouseExited(int x, int y) {

}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h) {

}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg) {

}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo) {

}
