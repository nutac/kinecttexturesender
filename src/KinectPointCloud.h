#pragma once

#include "ofMain.h"
#include "ofxNDI.h"

class KinectPointCloud {
public:
    KinectPointCloud();
    void setup(int width, int height, string ndiSenderName);
    void update(ofPixels_<unsigned short>& depthPixels, int _near, int _far, int depthMax, int step, float pointSize);
    void draw(int x, int y);
    void send();
private:
    ofMesh pointCloud;
    ofFbo fbo;
    ofxNDIsender ndiSender;
    float _pointSize;
    bool _showMesh;

};
