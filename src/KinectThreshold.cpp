#include "KinectThreshold.h"

KinectThreshold::KinectThreshold() {
}

void KinectThreshold::setup(int width, int height, string ndiSenderName) {
    processedPixels.allocate(width, height, OF_IMAGE_GRAYSCALE);
    processedTexture.allocate(processedPixels);

    senderName = ndiSenderName;
    fbo.allocate(width, height, GL_RGBA);
    ndiSender.SetReadback();
    ndiSender.SetAsync();
    ndiSender.CreateSender(senderName.c_str(), width, height);
}

void KinectThreshold::update(ofPixels_<unsigned short>& depthPixels, int _near, int _far) {
    int _near2 = _near* 10;
    int _far2 = _far * 10;
    int height = depthPixels.getHeight();
    int width = depthPixels.getWidth();

    for (int y = 0; y < height; y++) {
        for (int x = 0; x < width; x++) {
            unsigned short depthValue = depthPixels.getColor(x, y).getBrightness();
            if (depthValue > _near2 && depthValue < _far2) {
                int col = ofMap(depthValue, _far2, _near2, 50, 255);
                processedPixels.setColor(x, y, col);
            }
            else {
                processedPixels.setColor(x, y, ofColor(0));
            }
        }
    }
    processedTexture.loadData(processedPixels);
}

void KinectThreshold::draw(int x, int y) {
    fbo.begin();
    processedTexture.draw(0, 0);
    fbo.end();
    fbo.draw(x, y);
}

void KinectThreshold::send() {
    ndiSender.SendImage(fbo);
}
