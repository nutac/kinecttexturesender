#pragma once

#include "ofMain.h"
#include "ofxNDI.h"

class KinectThreshold {
public:
    KinectThreshold();

    void setup(int width, int height, string ndiSenderName);
    //void update(ofPixels_<unsigned short>& depthPixels, glm::vec2 threshold);
    void update(ofPixels_<unsigned short>& depthPixels,int _near, int _far);

    void draw(int x, int y);
    void send();

    ofTexture processedTexture;
private:
    ofPixels processedPixels;
    ofFbo fbo;

    ofxNDIsender ndiSender;
    string senderName;
};
