#pragma once

#include "ofMain.h"
#include "ofxNDI.h"
#include "ofxGui.h"

#include "ofxFlowTools.h"
using namespace flowTools;


class FluidFlow {
public:
	FluidFlow();

	void setup(int width, int height, string ndiSenderName);
	//void update(ofPixels_<unsigned short>& depthPixels, glm::vec2 threshold);
	void update(ofPixels_<unsigned short>& depthPixels, int _near, int _far);

	void draw(int x, int y, bool showGui);
	void send();

	ofTexture processedTexture;
	int w, h;

	ftFluidFlow fluidSimulation;
	ofParameterGroup visualizationParameters;
	ofParameter<int> visualizationMode;
	void setupFluidSimulation(int width, int height);

	ftOpticalFlow opticalFlow;
	ftCombinedBridgeFlow combinedBridgeFlow;

	ofxPanel			gui;
	//bool showGui =false;
	ofFbo rgbaFbo;




private:
	ofPixels processedPixels;
	ofFbo fbo;

	ofxNDIsender ndiSender;
	string senderName;
};
